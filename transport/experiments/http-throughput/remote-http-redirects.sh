#!/bin/bash

# Usage: ./remote-http-redirects.sh [OSS] [METHOD] [NUM OPEN REDIRECTS] [OUTPUT_FILENAME]
#
# Tests an HTPP  download over a single redirect session. If OUTPUT_FILENAME is
# supplied, appends the time measurement to that file.

. ../common.sh

DATA_FILE_NAME="$REDIRECTS_DIR/dump"
OSS="$1"
METHOD="$2"
REPEAT="$3"
OUTPUT_FILENAME="$4"

echo "********"${OSS}"/"${METHOD}"*************"
# Declare an array.
declare -a PIDS_TO_KILL
stop() {
	if [ -n "${PIDS_TO_KILL[*]}" ]; then
		echo "Kill pids ${PIDS_TO_KILL[@]}."
		kill "${PIDS_TO_KILL[@]}"
	fi
	echo "Delete data file."
	rm -f "$DATA_FILE_NAME"
	exit
}
trap stop EXIT

echo "Start SOCKS forwarder."
socat tcp-listen:5010,reuseaddr,fork socks4a:tor1.bamsoftware.com:tor1.bamsoftware.com:5003,socksport=5001 &
PIDS_TO_KILL+=($!)
visible_sleep 1

echo "Start client transport plugin."
python2.7 "$REDIRECTS_DIR"/redirects-client --http=:5002 --return=tor1.bamsoftware.com:5002 --socks=0.0.0.0:5001 --oss="${OSS}"/"${METHOD}" -d > clientplugin.log 2>&1 &
PIDS_TO_KILL+=($!)
visible_sleep 1

echo "Start server transport plugin."
python2.7 "$REDIRECTS_DIR"/redirects-server --http=:5003 tor1.bamsoftware.com:5004 -d > serverplugin.log 2>&1 &
PIDS_TO_KILL+=($!)
visible_sleep 1

echo "Start HTTP proxy."
ncat --proxy-type http -l 5004 &
PIDS_TO_KILL+=($!)

# Let Tor bootstrap.
visible_sleep 1

command="http_proxy=http://localhost:5010 wget http://torperf.torproject.org/.5mbfile --wait=0 --waitretry=0 -t 1000 -O "
multi_command=${command}${DATA_FILE_NAME}

#for (( i=2; i <= ${REPEAT}; i++ ))
#do
#	next_command=" &"";"${command}${DATA_FILE_NAME}${i}
#	multi_command=${multi_command}${next_command}
#done

echo ${multi_command}
if [ -n "$OUTPUT_FILENAME" ]; then
	real_time "${multi_command}" >> "$OUTPUT_FILENAME"
else
	real_time "${multi_command}"
fi
