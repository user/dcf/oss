#!/bin/bash

# Usage: ./remote-tor-constant.sh [OUTPUT_FILENAME]
#
# Tests a Tor download over a single redirect session. If OUTPUT_FILENAME is
# supplied, appends the time measurement to that file.

. ../common.sh

DATA_FILE_NAME="$REDIRECTS_DIR/dump"
OSS="$1"
METHOD="$2"
OUTPUT_FILENAME="$3"

echo "********"${OSS}"/"${METHOD}"*************"
# Declare an array.
declare -a PIDS_TO_KILL
stop() {
	if [ -n "${PIDS_TO_KILL[*]}" ]; then
		echo "Kill pids ${PIDS_TO_KILL[@]}."
		kill "${PIDS_TO_KILL[@]}"
	fi
	echo "Delete data file."
	rm -f "$DATA_FILE_NAME"
	exit
}
trap stop EXIT

echo "Start client transport plugin."
python2.7 "$REDIRECTS_DIR"/redirects-client --http=:5002 --return=tor1.bamsoftware.com:5002 --socks=0.0.0.0:5001 --oss="${OSS}"/"${METHOD}" -d > clientplugin.log 2>&1 &
PIDS_TO_KILL+=($!)
visible_sleep 1

echo "Start server transport plugin."
python2.7 "$REDIRECTS_DIR"/redirects-server --http=:5003 tor1.bamsoftware.com:9001 -d > serverplugin.log 2>&1 &
PIDS_TO_KILL+=($!)
visible_sleep 1

echo "Start Tor."
"$TOR" -f "$REDIRECTS_DIR"/experiments/throughput/torrc.redirects &
PIDS_TO_KILL+=($!)

# Let Tor bootstrap.
visible_sleep 75

if [ -n "$OUTPUT_FILENAME" ]; then
	real_time torify wget http://torperf.torproject.org/.5mbfile --wait=0 --waitretry=0 -c -t 1000 -O "$DATA_FILE_NAME" >> "$OUTPUT_FILENAME"
else
	real_time torify wget http://torperf.torproject.org/.5mbfile --wait=0 --waitretry=0 -c -t 1000 -O "$DATA_FILE_NAME"
fi
