#!/bin/bash

# Usage: ./throughput-all.sh [-n NUM_ITERATIONS]
#
# Runs the switching experiment scripts several times and stores the results in

. ../common.sh

NUM_ITERATIONS=1

while getopts "n:" OPTNAME; do
	if [ "$OPTNAME" == n ]; then
		NUM_ITERATIONS="$OPTARG"
	fi
done

DATE="$(date --iso)"

> "remote-tor-direct-$DATE.log"
#repeat $NUM_ITERATIONS ./remote-tor-direct.sh "remote-tor-direct-$DATE.log"

#oss_array: names of osses
#method_array: names of methods
#exec_array: which oss/method combination should we actually execute. 
#            Each column represents a method. Each row represnts an oss. 
#            Both according to their respective order in oss_array and method_array 

oss_array=( ref virustotal adsense pdfmyurl vurldissect w3c gomo twitter drweb wepawet )

method_array=( 300 301 302 303 307 frameset iframe jquerypostdiv jquerypostdoc onload metarefresh refreshheader )
exec_array=(    0   0   1   0   0   0        0      0             0             0      0           0 \
                0   0   1   0   0   0        0      0             0             0      0           0 \
                0   1   1   0   0   0        0      0             0             0      1           0 \
                0   0   1   0   0   1        1      0             1             1      0           1 \
                0   0   1   0   0   0        0      0             0             0      0           0 \
                0   0   1   0   0   0        0      0             0             0      0           0 \
                0   0   1   0   0   1        1      1             1             1      1           0 \
                0   1   0   0   0   1        0      1             1             0      1           0 \
                0   0   1   0   0   1        0      1             0             0      1           0 \
                0   0   1   0   0   1        0      1             0             0      1           0 )

> "remote-tor-single-redirect-session-$DATE.log"
index=0
for i in "${exec_array[@]}"
do
	oss_index=${index}/${#method_array[*]}
	#echo ${oss_index}
	method_index=${index}%${#method_array[*]}
	#echo ${method_index}
	if [ "$i" -eq "1" ] ; then
		echo ${oss_array[${oss_index}]}"/"${method_array[${method_index}]} >> "remote-tor-single-redirect-session-$DATE.log"
        	repeat $NUM_ITERATIONS ./remote-tor-constant.sh ${oss_array[${oss_index}]} ${method_array[${method_index}]} 
"remote-tor-single-redirect-session-$DATE.log"
	fi
	index=$((${index}+1))
done
