import cgi
import json
import urllib
import urlparse

class Redirect300(object):
    name = "300"
    description = "300 redirect"
    type = "GET"
    @staticmethod
    def redirect(handler, url):
        handler.send_response(300)
        handler.send_header("Location", url)
        handler.end_headers()

class Redirect301(object):
    name = "301"
    description = "301 redirect"
    type = "GET"
    @staticmethod
    def redirect(handler, url):
        handler.send_response(301)
        handler.send_header("Location", url)
        handler.end_headers()

class Redirect302(object):
    name = "302"
    description = "302 redirect"
    type = "GET"
    @staticmethod
    def redirect(handler, url):
        handler.send_response(302)
        handler.send_header("Location", url)
        handler.end_headers()

class Redirect303(object):
    name = "303"
    description = "303 redirect"
    type = "GET"
    @staticmethod
    def redirect(handler, url):
        handler.send_response(303)
        handler.send_header("Location", url)
        handler.end_headers()

class Redirect307(object):
    name = "307"
    description = "307 redirect"
    type = "GET"
    @staticmethod
    def redirect(handler, url):
        handler.send_response(307)
        handler.send_header("Location", url)
        handler.end_headers()

class RedirectIframe(object):
    name = "iframe"
    description = "iframe redirect"
    type = "GET"
    @staticmethod
    def redirect(handler, url):
        handler.send_response(200)
        handler.send_header("Content-type", "text/html")
        handler.end_headers()
        handler.wfile.write("<html><body><iframe src=\"" + cgi.escape(url, True) + "\"></iframe></body></html>")

class RedirectFrameset(object):
    name = "frameset"
    description = "frameset redirect"
    type = "GET"
    @staticmethod
    def redirect(handler, url):
        handler.send_response(200)
        handler.send_header("Content-type", "text/html")
        handler.end_headers()
        handler.wfile.write("<html><frameset><frame src=\"" + cgi.escape(url, True) + "\"></frame></frameset></html>")

class RedirectMetaRefresh(object):
    name = "metarefresh"
    description = "redirect using the META directive"
    type = "GET"
    @staticmethod
    def redirect(handler, url):
        handler.send_response(200)
        handler.send_header("Content-type", "text/html")
        handler.end_headers()
        handler.wfile.write("<html><head><meta http-equiv=\"refresh\" content=\"0; url='" + cgi.escape(url, True) + "'\"></head><body></body></html>")

class RedirectRefreshHeader(object):
    name = "refreshheader"
    description = "redirect using the HTTP Refresh header"
    type = "GET"
    @staticmethod
    def redirect(handler, url):
        handler.send_response(200)
        handler.send_header("Refresh", "0; url='" + cgi.escape(url, True) + "'")
        handler.send_header("Content-type", "text/html")
        handler.end_headers()
        handler.wfile.write("<html><body></body></html>")

class RedirectOnLoad(object):
    name = "onload"
    description = "JavaScript submit of a form on onload"
    type = "POST"
    @staticmethod
    def redirect(handler, url):
        handler.send_response(200)
        handler.send_header("Content-type", "text/html")
        handler.end_headers()
        #Extract and remove the data parameter
        parts = list(urlparse.urlparse(url))
        qsl = list(cgi.parse_qsl(parts[4], keep_blank_values=True))
        data = ""
        for k, v in qsl:
            if k == "data":
                data = v
                qsl.remove((k,v))
        parts[4] = urllib.urlencode(qsl)
        url = urlparse.urlunparse(parts)

        handler.wfile.write("""\
<html>
  <body onload="document.redirect.submit();">
    <form name="redirect" action=\"""" + cgi.escape(url, True) + """\" method="post">
      <input type="hidden" name="data" value=\"""" + cgi.escape(data, True) + """\"/>
    </form>
  </body>
</html>
""")

class RedirectXMLHttpRequest(object):
    name = "xhr"
    description = "XMLHttpRequest POST"
    type = "POST"
    @staticmethod
    def redirect(handler, url):
        handler.send_response(200)
        handler.send_header("Content-type", "text/html")
        handler.send_header("Access-Control-Allow-Origin", "*")
        handler.end_headers()
        # Extract and remove the data parameter.
        parts = list(urlparse.urlparse(url))
        qsl = list(cgi.parse_qsl(parts[4], keep_blank_values=True))
        data = ""
        for k, v in qsl:
            if k == "data":
                data = v
                qsl.remove((k,v))
        parts[4] = urllib.urlencode(qsl)
        url = urlparse.urlunparse(parts)

        handler.wfile.write("""\
<!DOCTYPE html>
<html>
  <body>
    <script type="text/javascript">
xhr = new XMLHttpRequest();
xhr.open("POST", """ + cgi.escape(json.dumps(url)) + """);
xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
xhr.onreadystatechange = function() {
    if (xhr.readyState == xhr.DONE && xhr.status == 200) {
        document.open();
        document.write(xhr.responseText);
        document.close();
    }
};
xhr.send(""" + cgi.escape(json.dumps(urllib.urlencode({"data": data}))) + """);
    </script>
  </body>
</html>
""")

class RedirectJQueryPostDoc(object):
    name = "jquerypostdoc"
    description = "redirect using a POST XMLHttpRequest and rewrite the entire document"
    type = "POST"
    @staticmethod
    def redirect(handler, url):
        handler.send_response(200)
        handler.send_header("Content-type", "text/html")
        handler.send_header("Access-Control-Allow-Origin", "*")
        handler.end_headers()
        #Extract and remove the data parameter
        parts = list(urlparse.urlparse(url))
        qsl = list(cgi.parse_qsl(parts[4], keep_blank_values=True))
        data = ""
        for k, v in qsl:
            if k == "data":
                data = v
                qsl.remove((k,v))
        parts[4] = urllib.urlencode(qsl)
        url = urlparse.urlunparse(parts)

        handler.wfile.write("""
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="#">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
  </head>
  <body>
    <div id="content"></div>
    <script type="text/javascript">
      $(document).ready(function() {
        $.post('""" + cgi.escape(url, True) + """', { data: \"""" + data + """\" }, function(returned_data)
          {var doc = document.open("text/html","replace");doc.write(returned_data);doc.close();});
      });
    </script>
  </body>
</html>
""")

class RedirectJQueryPostDiv(object):
    name = "jquerypostdiv"
    description = "redirect using a POST XMLHttpRequest  and rewrite a div"
    type = "POST"
    @staticmethod
    def redirect(handler, url):
        handler.send_response(200)
        handler.send_header("Content-type", "text/html")
        handler.send_header("Access-Control-Allow-Origin", "*")
        handler.end_headers()
        #Extract and remove the data parameter
        parts = list(urlparse.urlparse(url))
        qsl = list(cgi.parse_qsl(parts[4], keep_blank_values=True))
        data = ""
        for k, v in qsl:
            if k == "data":
                data = v
                qsl.remove((k,v))
        parts[4] = urllib.urlencode(qsl)
        url = urlparse.urlunparse(parts)

        handler.wfile.write("""
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="#">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
  </head>
  <body>
    <div id="content"></div>
    <script type="text/javascript">
      $(document).ready(function() {
        $.post('""" + cgi.escape(url, True) + """', { data: \"""" + data + """\" }, function(returned_data)
          {$('#content').html(returned_data);});
      });
    </script>
  </body>
</html>
""")

REDIRECTS_MAP = dict((x.name, x) for x in (
    Redirect300,
    Redirect301,
    Redirect302,
    Redirect303,
    Redirect307,
    RedirectIframe,
    RedirectFrameset,
    RedirectMetaRefresh,
    RedirectRefreshHeader,
    RedirectOnLoad,
    RedirectXMLHttpRequest,
    RedirectJQueryPostDoc,
    RedirectJQueryPostDiv
))
