OSS_LIST="\
    adsense \
    drweb \
    gomo \
    netrenderer \
    novirusthanks \
    pdfmyurl \
    ref \
    twitter \
    virustotal \
    vurldissect \
    w3c \
"
METHOD_LIST="\
    300 \
    301 \
    302 \
    303 \
    307 \
    frameset \
    iframe \
    metarefresh \
    onload \
    refreshheader \
    xhr \
"

for oss in $OSS_LIST; do
	for method in $METHOD_LIST; do
		echo -n "$oss/$method "
		grep -h '^  0 ' redirects-payload-results/*/$oss-$method | awk 'BEGIN{s=0}{s += $2; n+=1}END{printf("%.2f\n", s/n)}'
	done
done
