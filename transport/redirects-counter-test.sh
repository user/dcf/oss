#!/bin/bash

# facebook \
# goo.gl \
OSS_LIST="\
    adsense \
    gomo \
    netrenderer \
    novirusthanks \
    pdfmyurl \
    virustotal \
    vurldissect \
    w3c \
    wepawet \
"
METHOD_LIST="\
    300 \
    301 \
    302 \
    303 \
    307 \
    frameset \
    iframe \
    metarefresh \
    onload \
    refreshheader \
    xhr \
"

# Make a directory with a numeric suffix to avoid overwriting.
function make_directory() {
	basename="$1"
	mkdir -p $(dirname "$basename")
	N=0
	dirname="$basename"
	while ! mkdir "$dirname"; do
		N=$((N+1))
		dirname="$basename".$N
	done
	echo "$dirname"
}

results_dir=$(make_directory "redirects-counter-results/$(date --iso)")

echo "Saving results in $results_dir."

MYIP=$(./myip.py -4)

for method in $METHOD_LIST; do
	for oss in $OSS_LIST; do
		echo $oss/$method
		# ./redirects-counter --oss=$oss/$method --return $MYIP --peer tor1.bamsoftware.com:8000 2>&1 | tee $results_dir/$oss-$method
		# Use this instead to bounce off yourself.
		python2.7 ./redirects-counter --oss=$oss/$method --return $MYIP 2>&1 | tee $results_dir/$oss-$method
	done
done
