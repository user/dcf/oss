#!/usr/bin/env python

import getopt
import os
import select
import socket
import struct
import sys
import time
import urlparse

from common import *
import osslib
import redirectslib

DEFAULT_HTTP_ADDR = ("0.0.0.0", 7000)
DEFAULT_SOCKS_ADDR = ("127.0.0.1", 5000)
DEFAULT_OSS_NAME = "manual"
DEFAULT_METHOD_NAME = "302"

REFRESH_INTERVAL = 1.0

# Mapping from stream ids to existing connections.
STREAMS = {}

class options(object):
    http_address = None
    socks_address = None
    return_address = None
    oss_class = None
    redirect_class = None

def usage(f = sys.stdout):
    print >> f, """\
Usage: %(progname)s [OPTIONS]
Redirects-based pluggable transport client.
""" % {
    "progname": sys.argv[0],
}
    print >> f, """\
The --oss option control which scanning service to use. Possibilities for
scanning service are:\
"""
    for name, oss in sorted(osslib.OSS_MAP.items()):
        print >> f, "    " + name + ": " + oss.description
    print >> f, """\
Possibilities for redirect method are:\
"""
    for name, redirect in sorted(redirectslib.REDIRECTS_MAP.items()):
        print >> f, "    " + name + ": " + redirect.description
    print >> f, """\

  -d, --debug             show debug output.
  -h, --help              show this help.
  --http [ADDR][:PORT]    listen for HTTP requests on the given address
                            (default %(http_addr)s).
  --oss [OSS][/METHOD]    use the given scanning service and redirect method
                            (default "%(oss_name)s/%(method_name)s").
  --return [ADDR][:PORT]  use URL as the return-to URL sent to the server
                            transport plugin (default guessed, may not work
                            with NAT).
  --socks ADDR[:PORT]     listen for SOCKS requests on the given address
                            (default %(socks_addr)s).\
""" % {
    "http_addr": format_addr(DEFAULT_HTTP_ADDR),
    "socks_addr": format_addr(DEFAULT_SOCKS_ADDR),
    "oss_name": DEFAULT_OSS_NAME,
    "method_name": DEFAULT_METHOD_NAME,
}

def listen(addr):
    host, port = socket.getnameinfo(addr, 0)
    addrinfo = socket.getaddrinfo(host, port, 0, socket.SOCK_STREAM, socket.IPPROTO_TCP)[0]
    s = socket.socket(addrinfo[0], addrinfo[1], addrinfo[2])
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(addr)
    s.listen(1)
    return s

class Handler(StreamHandler):
    def send_redirect(self, stream, payload=""):
        if stream.unacked > 0:
            logdebug("send_redirect case 1")
            data = stream.rbuf[:stream.unacked]
            assert len(data) == stream.unacked, (len(data), stream.unacked, len(stream.rbuf))
            self.redirect_data(stream.redirect_class.redirect, stream, data)
        elif len(stream.rbuf) == 0 and stream.rbuf.is_shutdown and not stream.rbuf.fin_sent:
            logdebug("send_redirect case 2")
            self.redirect_fin(stream.redirect_class.redirect, stream)
            stream.rbuf.fin_sent = True
        elif payload or len(stream.rbuf) > 0:
            logdebug("send_redirect case 3")
            data = stream.rbuf[:int(stream.chunk_size)]
            stream.unacked = len(data)
            self.redirect_data(stream.redirect_class.redirect, stream, data)
        else:
            logdebug("send_redirect case 4")
            self.send_response(204)
            self.end_headers()

    def do_stream_request(self, stream_id, seq, ack):
        stream = STREAMS.get(stream_id)
        if stream is None:
            logdebug("stream %r doesn't exist?" % stream_id)
            self.send_error(400)
            return

        # peer claims to have sent seq bytes, not including this payload.
        # peer claims to have seen ack of our bytes.

        logdebug("expecting seq %d == %d,  ack %d == %d == %d + %d" %
            (seq, stream.ack, ack, stream.seq + stream.unacked, stream.seq, stream.unacked))
        if seq != stream.ack:
            # We are not expecting data with this seq number.
            self.send_redirect(stream)
            return

        payload = self.extract_payload()
        logdebug("got payload of %d bytes" % len(payload))
        stream.wbuf.push(payload)
        stream.ack = seq + len(payload)

        # if payload:
        stream.refresh_interval = 2.0

        if ack > stream.seq + stream.unacked or ack < stream.seq:
            # Claims to ack more bytes than we have sent.
            logdebug("stream %r misplaced ack?" % stream_id)
            self.send_error(400)
            return
        stream.rbuf.advance(ack - stream.seq)
        stream.unacked -= ack - stream.seq
        stream.seq = ack

        if self.extract_fin():
            # This means end of stream.
            stream.wbuf.will_shutdown = True

        self.send_redirect(stream, payload)

class Server(BaseHTTPServer.HTTPServer):
    allow_reuse_address = True

def gen_stream_id():
    return os.urandom(4).encode("hex")


def grab_string(s, pos):
    """Grab a NUL-terminated string from the given string, starting at the given
    offset. Return (pos, str) tuple, or (pos, None) on error."""
    i = pos
    while i < len(s):
        if s[i] == '\0':
            return (i + 1, s[pos:i])
        i += 1
    return pos, None

# http://ftp.icm.edu.pl/packages/socks/socks4/SOCKS4.protocol
# https://en.wikipedia.org/wiki/SOCKS#SOCKS4a
def parse_socks_request(data):
    """Parse the 8-byte SOCKS header at the beginning of data. Returns a
    (dest, port) tuple. Raises ValueError on error."""
    try:
        ver, cmd, dport, o1, o2, o3, o4 = struct.unpack(">BBHBBBB", data[:8])
    except struct.error:
        raise ValueError("Couldn't unpack SOCKS4 header")
    if ver != 4:
        raise ValueError("Wrong SOCKS version (%d)" % ver)
    if cmd != 1:
        raise ValueError("Wrong SOCKS command (%d)" % cmd)
    pos, userid = grab_string(data, 8)
    if userid is None:
        raise ValueError("Couldn't read userid")
    if o1 == 0 and o2 == 0 and o3 == 0 and o4 != 0:
        pos, dest = grab_string(data, pos)
        if dest is None:
            raise ValueError("Couldn't read destination")
    else:
        dest = "%d.%d.%d.%d" % (o1, o2, o3, o4)
    return dest, dport

def handle_socks_request(fd):
    try:
        addr = fd.getpeername()
        data = fd.recv(100)
    except socket.error, e:
        logdebug("Socket error from SOCKS-pending: %s" % repr(str(e)))
        return None
    try:
        dest_addr = parse_socks_request(data)
    except ValueError, e:
        logdebug("Error parsing SOCKS request: %s." % str(e))
        # Error reply.
        fd.sendall(struct.pack(">BBHBBBB", 0, 91, 0, 0, 0, 0, 0))
        return None
    logdebug("Got SOCKS request for %s." % format_addr(dest_addr))
    fd.sendall(struct.pack(">BBHBBBB", 0, 90, dest_addr[1], 127, 0, 0, 1))

    stream = Stream(fd, gen_stream_id())
    stream.peer_url = urlparse.urlunparse(["http", format_netloc(dest_addr), "", "", "", ""])
    stream.oss = options.oss_class()
    STREAMS[stream.id] = stream

    logdebug("New stream %s." % stream)
    stream_initiate(stream)

def stream_initiate(stream):
    stream.last_send_time = time.time()
    stream.redirect_class = options.redirect_class
    return stream.oss.initiate(stream, options.return_address)

def stream_refresh(stream):
    logdebug("refresh")
    stream.refresh_interval = 100.0 # min(5, stream.refresh_interval * 1.5)
    stream_initiate(stream)

def stream_read(stream):
    data = stream.s.recv(4096)
    logdebug("read %d bytes from %s" % (len(data), stream))
    if data:
        stream.rbuf.push(data)
        # stream.refresh_interval = 1.0
    else:
        logdebug("shutdown %s rd" % stream)
        try:
            stream.s.shutdown(socket.SHUT_RD)
        except socket.error:
            pass
        stream.rbuf.shutdown()

def stream_write(stream):
    try:
        n = stream.s.send(stream.wbuf[:4096])
        logdebug("write %d bytes to %s" % (n, stream))
        stream.wbuf.advance(n)
    except socket.error:
        stream.wbuf.buf = ""
        stream.wbuf.shutdown()
        return
    if len(stream.wbuf) == 0 and stream.wbuf.will_shutdown:
        logdebug("shutdown %s wr" % stream)
        stream.s.shutdown(socket.SHUT_WR)
        stream.wbuf.shutdown()

def guess_return_address(http_address):
    return (guess_external_ip(), http_address[1])

if __name__ == "__main__":
    options.http_address = DEFAULT_HTTP_ADDR
    options.socks_address = DEFAULT_SOCKS_ADDR
    oss_name = DEFAULT_OSS_NAME
    method_name = DEFAULT_METHOD_NAME
    return_address_spec = ":"

    opts, args = getopt.gnu_getopt(sys.argv[1:], "dh", ["debug", "help", "http=", "oss=", "return=", "socks="])
    for o, a in opts:
        if o == "-d" or o == "--debug":
            setdebug(True)
        elif o == "-h" or o == "--help":
            usage()
            sys.exit()
        elif o == "--http":
            options.http_address = parse_addr_spec(a, defhost="0.0.0.0", defport=DEFAULT_HTTP_ADDR[1])
        elif o == "--oss":
            oss_name, method_name = parse_oss_spec(a, oss_name, method_name)
        elif o == "--socks":
            options.socks_address = parse_addr_spec(a, defhost="127.0.0.1", defport=DEFAULT_SOCKS_ADDR[1])
        elif o == "--return":
            return_address_spec = a

    if len(args) != 0:
        usage(sys.stderr)
        sys.exit(1)

    options.return_address = parse_addr_spec(return_address_spec,
        defhost=guess_external_ip(), defport=options.http_address[1])
    logdebug("Guessed return address as %s; use --return to change." % format_addr(options.return_address))

    options.oss_class = osslib.OSS_MAP[oss_name]
    options.redirect_class = redirectslib.REDIRECTS_MAP[method_name]

    logdebug("SOCKS listening on %s." % format_addr(options.socks_address))
    socks_listen = listen(options.socks_address)

    http_server = Server(options.http_address, Handler)
    logdebug("HTTP listening on %s." % format_addr(options.http_address))

    while True:
        read_streams = [socks_listen, http_server]
        read_streams.extend([x for x in STREAMS.values() if not x.rbuf.is_shutdown])
        write_streams = [x for x in STREAMS.values() if x.wbuf.buf or (x.wbuf.will_shutdown and not x.wbuf.is_shutdown)]
        rset, wset, _ = select.select(read_streams, write_streams, [], REFRESH_INTERVAL)
        for fd in rset:
            if fd == socks_listen:
                c, addr = fd.accept()
                logdebug("SOCKS connection from %s." % format_sockaddr(addr))
                handle_socks_request(c)
            elif fd == http_server:
                logdebug("readable http")
                fd.handle_request()
            else:
                logdebug("readable client")
                stream_read(fd)
        for fd in wset:
            logdebug("writable client")
            stream_write(fd)

        for stream in STREAMS.values():
            if stream.rbuf.fin_sent and stream.wbuf.is_shutdown:
                logdebug("deleting stream %s" % stream.id)
                del STREAMS[stream.id]
            now = time.time()
            if now - stream.last_send_time > stream.refresh_interval:
                stream_refresh(stream)
