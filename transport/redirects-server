#!/usr/bin/env python

import getopt
import select
import socket
import sys

import redirectslib
from common import *

# Mapping from stream ids to existing connections.
STREAMS = {}

DEFAULT_HTTP_ADDR = ("0.0.0.0", 8000)
DEFAULT_METHOD_NAME = "302"

class options(object):
    http_address = None
    connect_address = None

def usage(f = sys.stdout):
    print >> f, """\
Usage: %(progname)s [OPTIONS] HOST:PORT
Redirects-based pluggable transport server. All redirects-based streams are
forwarded to HOST:PORT.

  -d, --debug             show debug output.
  -h, --help              show this help.
  --http [ADDR][:PORT]    listen for HTTP requests on the given address
                            (default %(http_addr)s).\
""" % {
    "progname": sys.argv[0],
    "http_addr": format_addr(DEFAULT_HTTP_ADDR),
}

def connect(addr):
    host, port = socket.getnameinfo(addr, 0)
    addrinfo = socket.getaddrinfo(host, port, 0, socket.SOCK_STREAM, socket.IPPROTO_TCP)[0]
    s = socket.socket(addrinfo[0], addrinfo[1], addrinfo[2])
    s.connect(addrinfo[4])
    return s

class Handler(StreamHandler):
    def send_redirect(self, stream, payload=""):
        if stream.unacked > 0:
            logdebug("send_redirect case 1")
            data = stream.rbuf[:stream.unacked]
            assert len(data) == stream.unacked, (len(data), stream.unacked, len(stream.rbuf))
            self.redirect_data(stream.redirect_class.redirect, stream, data)
        elif len(stream.rbuf) == 0 and stream.rbuf.is_shutdown:
            logdebug("send_redirect case 2")
            self.redirect_fin(stream.redirect_class.redirect, stream)
            stream.rbuf.fin_sent = True
        else:
            logdebug("send_redirect case 3")
            data = stream.rbuf[:int(stream.chunk_size)]
            stream.unacked = len(data)
            self.redirect_data(stream.redirect_class.redirect, stream, data)

    def do_stream_request(self, stream_id, seq, ack):
        stream = STREAMS.get(stream_id)
        if stream is not None:
            logdebug("stream %r already exists" % stream_id)
        elif seq == 0 and ack == 0:
            logdebug("stream %r doesn't yet exist" % stream_id)
            return_url = self.extract_return_url()
            if return_url is None:
                self.send_error(400)
                return
            stream = Stream(connect(options.connect_address), stream_id)
            STREAMS[stream.id] = stream
            stream.peer_url = return_url
        else:
            logdebug("stale stream request %r" % stream_id)
            self.send_error(400)
            return

        # Initialize or update stream method.
        method_name = self.extract_method_name()
        if method_name:
            stream.redirect_class = redirectslib.REDIRECTS_MAP.get(method_name)
        if stream.redirect_class is None:
            logdebug("don't know method \"%s\"" % method_name)
            self.send_error(400)
            return

        # Initialize or update allowed chunk size.
        chunk_size = self.extract_chunk_size()
        if chunk_size:
            stream.chunk_size = chunk_size
        else:
            logdebug("don't know allowed chunk size. Using the default value of %d." % GET_CHUNK_SIZE)
            stream.chunk_size = GET_CHUNK_SIZE

        logdebug("expecting seq %d == %d,  ack %d == %d == %d + %d" %
            (seq, stream.ack, ack, stream.seq + stream.unacked, stream.seq, stream.unacked))
        if seq != stream.ack:
            # We are not expecting data with this seq number.
            self.send_redirect(stream)
            return

        payload = self.extract_payload()
        logdebug("got payload of %d bytes" % len(payload))
        stream.wbuf.push(payload)
        stream.ack = seq + len(payload)

        if ack > stream.seq + stream.unacked or ack < stream.seq:
            # Claims to ack more bytes than we have sent.
            logdebug("stream %r misplaced ack?" % stream_id)
            self.send_error(400)
            return
        stream.rbuf.advance(ack - stream.seq)
        stream.unacked -= ack - stream.seq
        stream.seq = ack

        if self.extract_fin():
            # This means end of stream.
            stream.wbuf.will_shutdown = True

        self.send_redirect(stream, payload)

class Server(BaseHTTPServer.HTTPServer):
    allow_reuse_address = True

def stream_read(stream):
    data = stream.s.recv(4096)
    logdebug("read %d bytes from %s" % (len(data), stream))
    if data:
        stream.rbuf.push(data)
    else:
        logdebug("shutdown %s rd" % stream)
        stream.s.shutdown(socket.SHUT_RD)
        stream.rbuf.shutdown()

def stream_write(stream):
    n = stream.s.send(stream.wbuf[:4096])
    logdebug("write %d bytes to %s" % (n, stream))
    stream.wbuf.advance(n)
    if len(stream.wbuf) == 0 and stream.wbuf.will_shutdown:
        logdebug("shutdown %s wr" % stream)
        stream.s.shutdown(socket.SHUT_WR)
        stream.wbuf.shutdown()

if __name__ == "__main__":
    options.http_address = DEFAULT_HTTP_ADDR

    opts, args = getopt.gnu_getopt(sys.argv[1:], "dh", ["debug", "help", "http="])
    for o, a in opts:
        if o == "-d" or o == "--debug":
            setdebug(True)
        elif o == "-h" or o == "--help":
            usage()
            sys.exit()
        elif o == "--http":
            options.http_address = parse_addr_spec(a, defhost="0.0.0.0", defport=DEFAULT_HTTP_ADDR[1])

    if len(args) != 1:
        usage(sys.stderr)
        sys.exit(1)

    options.connect_address = parse_addr_spec(args[0])

    http_server = Server(options.http_address, Handler)
    logdebug("HTTP listening on %s." % format_addr(options.http_address))

    while True:
        read_streams = [http_server]
        read_streams.extend([x for x in STREAMS.values() if not x.rbuf.is_shutdown])
        write_streams = [x for x in STREAMS.values() if x.wbuf.buf or (x.wbuf.will_shutdown and not x.wbuf.is_shutdown)]
        rset, wset, _ = select.select(read_streams, write_streams, [])
        for fd in rset:
            if fd == http_server:
                logdebug("readable http")
                fd.handle_request()
            else:
                logdebug("readable stream %s" % fd)
                try:
                    stream_read(fd)
                except socket.error, e:
                    logdebug("stream_read error %s" % str(e))
                    del STREAMS[fd.id]
        for fd in wset:
            logdebug("writable stream %s" % fd)
            try:
                stream_write(fd)
            except socket.error, e:
                logdebug("stream_write error %s" % str(e))
                del STREAMS[fd.id]

        for stream in STREAMS.values():
            if stream.rbuf.fin_sent and stream.wbuf.is_shutdown:
                logdebug("deleting stream %s" % stream.id)
                del STREAMS[stream.id]
