/*
	A simple HTTP server to run the scan CGI for testing purposes.
*/

package main

import "log"
import "net/http"
import "net/http/cgi"

func main() {
	scanHandler := &cgi.Handler{
		Path: "./scan",
	}
	http.Handle("/scan", scanHandler)

	err := http.ListenAndServe(":http", nil)
	if err != nil {
		log.Fatal(err)
	}
}
