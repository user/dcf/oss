/*
	A simple HTTP server to run the refresh CGI for testing purposes.
*/

package main

import "log"
import "net/http"
import "net/http/cgi"

func main() {
	refreshHandler := &cgi.Handler{
		Path: "./refresh",
	}
	http.Handle("/refresh", refreshHandler)

	err := http.ListenAndServe(":http", nil)
	if err != nil {
		log.Fatal(err)
	}
}
